package com.phkl.businessrules.claims.common;

public class ExchangeRate {
    // Always HKD
    private String toCurrency;
    private String fromCurrency;
    private double rate;

    public String getFromCurrency() {
	return fromCurrency;
    }

    public void setFromCurrency(String fromCurrency) {
	this.fromCurrency = fromCurrency;
    }

    public double getRate() {
	return rate;
    }

    public void setRate(double rate) {
	this.rate = rate;
    }

}
