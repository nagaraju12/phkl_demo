package com.phkl.businessrules.claims.common;

public class Message {
	private String code;
	private String type;
	private String severity;
	private String description;

	public Message(String code, String type, String severity, String description) {
		super();
		this.code = code;
		this.type = type;
		this.severity = severity;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static Message createMessage(String code, String type,
			String severity, String description) {
		Message message = new Message(code, type, severity, description);
		return message;

	}

}
