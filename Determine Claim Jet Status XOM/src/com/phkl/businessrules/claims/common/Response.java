package com.phkl.businessrules.claims.common;

import java.util.ArrayList;
import java.util.List;

public class Response {
	private List<Message> ruleMessages;

	protected List<Message> addMessageToMessages(Message message, List<Message> messages) {
		if (null == messages)
			messages = new ArrayList<Message>();
		messages.add(message);
		return messages;

	}

	public void addRuleMessages(String code, String type, String severity,
			String description, String dest) {
		Message message = null;
		if (dest.equals("RULE")) {
			message = new Message(code, type, severity, description);
			this.addMessageToMessages(message, this.getRuleMessages());
		}

	}

	public List<Message> getRuleMessages() {
		if (ruleMessages == null)
			ruleMessages = new ArrayList<Message>();
		return ruleMessages;
	}

	public void setRuleMessages(List<Message> ruleMessages) {
		this.ruleMessages = ruleMessages;
	}

	// public List<Message> getApplicationMessages() {
	// return applicationMessages;
	// }
	//
	// public void setApplicationMessages(List<Message> applicationMessages) {
	// this.applicationMessages = applicationMessages;
	// }
//	public static void main(String[] args) {
//		Response response = new Response();
//		response.addRuleMessages("1000", "App", "Info", "New Message", "RULE");
//		System.out.println(response.getRuleMessages().size());
//	}

}
