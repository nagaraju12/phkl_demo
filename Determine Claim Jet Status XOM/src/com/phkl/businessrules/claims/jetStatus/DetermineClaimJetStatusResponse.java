package com.phkl.businessrules.claims.jetStatus;

import ilog.rules.bom.annotations.BusinessType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.phkl.businessrules.claims.common.Response;
import com.phkl.businessrules.claims.domain.Claim;
import com.phkl.businessrules.claims.domain.ClaimRejectionDetail;
import com.phkl.businessrules.claims.domain.CostPersentileAssessment;
import com.phkl.businessrules.claims.domain.Diagnosis;
import com.phkl.businessrules.claims.domain.LOSPersentile;
import com.phkl.businessrules.claims.domain.Product;
import com.phkl.businessrules.claims.domain.Treatment;

public class DetermineClaimJetStatusResponse extends Response {

	private Claim claim = new Claim();
	private int lengthOfStayinDays;
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus")
	private String claimBilledAmountJETEligibility = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.JETStatus")
	private String claimJETStatus = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus")
	private String claimJETEligible = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus")
	private String claimProductJETEligible = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ValidityStatus")
	private String claimJETValidityAssessment = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.WastageAssessment")
	private String claimMedicalWastageAssessment = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus")
	private String claimComplexityAssessment = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus")
	private String eventComplexityAssessment = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus")
	private String diagnosisComplexityAssessment = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus")
	private String diagnosisComboComplexityAssessment = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus")
	private String treatmentComplexityAssessment = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus")
	private String claimHistoryComplexityAssessment = "";
	private int claimHighestSeverity = 0;

	private List<String> surgeryList = new ArrayList<String>();
	private List<CostPersentileAssessment> claimCostPercentileList = new ArrayList<CostPersentileAssessment>();

	public List<CostPersentileAssessment> getClaimCostPercentileList() {
		if (claimCostPercentileList == null)
			claimCostPercentileList = new ArrayList<CostPersentileAssessment>();
		return claimCostPercentileList;
	}

	private List<String> claimMedicalWastageAssessmentList = new ArrayList<String>();

	public List<String> getClaimMedicalWastageAssessmentList() {
		if (claimMedicalWastageAssessmentList == null)
			claimMedicalWastageAssessmentList = new ArrayList<String>();
		return claimMedicalWastageAssessmentList;
	}

	private List<Diagnosis> acuteDiagnosisList = new ArrayList<Diagnosis>();
	private List<Diagnosis> acuteOnlyDiagnosisList = new ArrayList<Diagnosis>();
	private List<Diagnosis> chronicDiagnosisList = new ArrayList<Diagnosis>();
	private List<Diagnosis> catastrophicDiagnosisList = new ArrayList<Diagnosis>();
	private List<Treatment> severity1SurgicalCodes = new ArrayList<Treatment>();
	private List<Treatment> severity2SurgicalCodes = new ArrayList<Treatment>();
	private List<Treatment> severity3SurgicalCodes = new ArrayList<Treatment>();
	private List<Treatment> severity4SurgicalCodes = new ArrayList<Treatment>();
	private List<Treatment> severity5SurgicalCodes = new ArrayList<Treatment>();
	private List<Treatment> severity6SurgicalCodes = new ArrayList<Treatment>();
	private List<Treatment> severity7SurgicalCodes = new ArrayList<Treatment>();

	@BusinessType("com.phkl.businessrules.claims.virtualdomain.EarlyClaimStatus")
	private String earlyClaimStatus = "";
	private String preExistingConditionStatus = "";

	@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus")
	private String claimPolicyandBenefitEligibilityStatus = "";

	private List<ClaimRejectionDetail> claimRejectionDetails = new ArrayList<ClaimRejectionDetail>();

	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus")
	private String policyIndividualExclusionStatus = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus")
	private String policyProductSpecificExclusionStatus = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus")
	private String policyAllProductExclusionStatus = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus")
	private String policyGeneralExclusionStatus = "";
	private String claimExclusionStatus = "";

	private String diagnosisJETEligibility = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.Assessment")
	private String treatmentLengthofStayAssessment = "";

	private List<LOSPersentile> treatmentLOSAssessmentList = new ArrayList<LOSPersentile>();
	private int treatmentLOS50thPercentileMax;
	private int treatmentLOS75thPercentileMax;
	private int treatmentLOS90thPercentileMax;
	private String surgicalCodeAssessment = "";
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.SurgeryAppropriateness")
	private String surgeryAppropriateness = "";

	@BusinessType("com.phkl.businessrules.claims.virtualdomain.SurgeryAppropriateness")
	private String nonSurgeryAppropriateness = "";

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.SurgeryAppropriateness") String getNonSurgeryAppropriateness() {
		return nonSurgeryAppropriateness;
	}

	public void setNonSurgeryAppropriateness(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.SurgeryAppropriateness") String nonSurgeryAppropriateness) {
		this.nonSurgeryAppropriateness = nonSurgeryAppropriateness;
	}

	@BusinessType("com.phkl.businessrules.claims.virtualdomain.Assessment")
	private String claimCostAssessment = "";

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.Assessment") String getClaimCostAssessment() {
		return claimCostAssessment;
	}

	public void setClaimCostAssessment(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.Assessment") String claimCostAssessment) {
		this.claimCostAssessment = claimCostAssessment;
	}

	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus")
	private String analyticModelWastagePrediction = "";

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String getAnalyticModelWastagePrediction() {
		return analyticModelWastagePrediction;
	}

	public void setAnalyticModelWastagePrediction(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String analyticModelWastagePrediction) {
		this.analyticModelWastagePrediction = analyticModelWastagePrediction;
	}

	@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus")
	private String analyticModelHospitalizationNecessityPrediction = "";

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String getAnalyticModelHospitalizationNecessityPrediction() {
		return analyticModelHospitalizationNecessityPrediction;
	}

	public void setAnalyticModelHospitalizationNecessityPrediction(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String analyticModelHospitalizationNecessityPrediction) {
		this.analyticModelHospitalizationNecessityPrediction = analyticModelHospitalizationNecessityPrediction;
	}

	@BusinessType("com.phkl.businessrules.claims.virtualdomain.SurgeryAppropriateness")
	private String treatmentLOSAssessment = "";
	private String treatmentHospitalizationAssessment = "";
	private List<String> medicalCenterGeographicAreas = new ArrayList<String>();
	private String claimTimePeriodinDays = "";
	private String claimTimePeriodinMonths = "";

	private List<Diagnosis> allDiagnosis = new ArrayList<Diagnosis>();
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.WithIn120daysOfPolicyInForceDate")
	private String claimWithin120daysofPolicyInForceDate = "";

	private String claimWithin12monthsofPolicyInForceDate = "";
	// Derived attribute : also present in Policy
	private Date policyInForceDate = new Date();
	private Date claimEarliestEventDate = new Date();

	// KPI Metrics
	private int noOfJetEligibleProducts = 0;
	private int noOfJetIneligibleProducts = 0;
	private int noOfSeverityOneTreatmentCode = 0;
	private int noOfSeverityTwoTreatmentCode = 0;
	private int noOfSeverityThreeTreatmentCode = 0;
	private int noOfSeverityFourTreatmentCode = 0;
	private int noOfSeverityFiveTreatmentCode = 0;
	private int noOfSeveritySixTreatmentCode = 0;
	private int noOfSeveritySevenTreatmentCode = 0;
	private int noOfTotalTreatmentCodes = 0;

	private int noOfAcuteOnlyDiagnosisCodes = 0;
	private int noOfAcuteDiagnosisCodes = 0;
	private int noOfChronicDiagnosisCodes = 0;
	private int noOfCatasthrophicDiagnosisCodes = 0;
	private int noOfTotalDiagnosisCodes = 0;
	private List<Product> eligibleProductList = new ArrayList<Product>();
	private List<Product> ineligibleProductList = new ArrayList<Product>();

	private List<String> surgeryAssessmentList = new ArrayList<String>();

	public int getNoOfJetEligibleProducts() {
		return noOfJetEligibleProducts;
	}

	public void setNoOfJetEligibleProducts(int noOfJetEligibleProducts) {
		this.noOfJetEligibleProducts = noOfJetEligibleProducts;
	}

	public int getNoOfJetIneligibleProducts() {
		return noOfJetIneligibleProducts;
	}

	public void setNoOfJetIneligibleProducts(int noOfJetIneligibleProducts) {
		this.noOfJetIneligibleProducts = noOfJetIneligibleProducts;
	}

	public int getNoOfSeverityOneTreatmentCode() {
		return noOfSeverityOneTreatmentCode;
	}

	public void setNoOfSeverityOneTreatmentCode(int noOfSeverityOneTreatmentCode) {
		this.noOfSeverityOneTreatmentCode = noOfSeverityOneTreatmentCode;
	}

	public int getNoOfSeverityTwoTreatmentCode() {
		return noOfSeverityTwoTreatmentCode;
	}

	public void setNoOfSeverityTwoTreatmentCode(int noOfSeverityTwoTreatmentCode) {
		this.noOfSeverityTwoTreatmentCode = noOfSeverityTwoTreatmentCode;
	}

	public int getNoOfSeverityThreeTreatmentCode() {
		return noOfSeverityThreeTreatmentCode;
	}

	public void setNoOfSeverityThreeTreatmentCode(
			int noOfSeverityThreeTreatmentCode) {
		this.noOfSeverityThreeTreatmentCode = noOfSeverityThreeTreatmentCode;
	}

	public int getNoOfSeverityFourTreatmentCode() {
		return noOfSeverityFourTreatmentCode;
	}

	public void setNoOfSeverityFourTreatmentCode(
			int noOfSeverityFourTreatmentCode) {
		this.noOfSeverityFourTreatmentCode = noOfSeverityFourTreatmentCode;
	}

	public int getNoOfSeverityFiveTreatmentCode() {
		return noOfSeverityFiveTreatmentCode;
	}

	public void setNoOfSeverityFiveTreatmentCode(
			int noOfSeverityFiveTreatmentCode) {
		this.noOfSeverityFiveTreatmentCode = noOfSeverityFiveTreatmentCode;
	}

	public int getNoOfSeveritySixTreatmentCode() {
		return noOfSeveritySixTreatmentCode;
	}

	public void setNoOfSeveritySixTreatmentCode(int noOfSeveritySixTreatmentCode) {
		this.noOfSeveritySixTreatmentCode = noOfSeveritySixTreatmentCode;
	}

	public int getNoOfSeveritySevenTreatmentCode() {
		return noOfSeveritySevenTreatmentCode;
	}

	public void setNoOfSeveritySevenTreatmentCode(
			int noOfSeveritySevenTreatmentCode) {
		this.noOfSeveritySevenTreatmentCode = noOfSeveritySevenTreatmentCode;
	}

	public int getNoOfAcuteOnlyDiagnosisCodes() {
		return noOfAcuteOnlyDiagnosisCodes;
	}

	public void setNoOfAcuteOnlyDiagnosisCodes(int noOfAcuteOnlyDiagnosisCodes) {
		this.noOfAcuteOnlyDiagnosisCodes = noOfAcuteOnlyDiagnosisCodes;
	}

	public int getNoOfAcuteDiagnosisCodes() {
		return noOfAcuteDiagnosisCodes;
	}

	public void setNoOfAcuteDiagnosisCodes(int noOfAcuteDiagnosisCodes) {
		this.noOfAcuteDiagnosisCodes = noOfAcuteDiagnosisCodes;
	}

	public int getNoOfChronicDiagnosisCodes() {
		return noOfChronicDiagnosisCodes;
	}

	public void setNoOfChronicDiagnosisCodes(int noOfChronicDiagnosisCodes) {
		this.noOfChronicDiagnosisCodes = noOfChronicDiagnosisCodes;
	}

	public int getNoOfCatasthrophicDiagnosisCodes() {
		return noOfCatasthrophicDiagnosisCodes;
	}

	public void setNoOfCatasthrophicDiagnosisCodes(
			int noOfCatasthrophicDiagnosisCodes) {
		this.noOfCatasthrophicDiagnosisCodes = noOfCatasthrophicDiagnosisCodes;
	}

	public int getNoOfTotalDiagnosisCodes() {
		return noOfTotalDiagnosisCodes;
	}

	public void setNoOfTotalDiagnosisCodes(int noOfTotalDiagnosisCodes) {
		this.noOfTotalDiagnosisCodes = noOfTotalDiagnosisCodes;
	}

	public int getLengthOfStayinDays() {
		return lengthOfStayinDays;
	}

	public void setLengthOfStayinDays(int lengthOfStayinDays) {
		this.lengthOfStayinDays = lengthOfStayinDays;
	}

	public List<Diagnosis> getAllDiagnosis() {
		if (allDiagnosis == null)
			allDiagnosis = new ArrayList<Diagnosis>();
		return allDiagnosis;
	}

	public String getClaimTimePeriodinDays() {
		return claimTimePeriodinDays;
	}

	public void setClaimTimePeriodinDays(String claimTimePeriodinDays) {
		this.claimTimePeriodinDays = claimTimePeriodinDays;
	}

	public String getClaimTimePeriodinMonths() {
		return claimTimePeriodinMonths;
	}

	public void setClaimTimePeriodinMonths(String claimTimePeriodinMonths) {
		this.claimTimePeriodinMonths = claimTimePeriodinMonths;
	}

	public Date getClaimEarliestEventDate() {
		return claimEarliestEventDate;
	}

	public void setClaimEarliestEventDate(Date claimEarliestEventDate) {
		this.claimEarliestEventDate = claimEarliestEventDate;
	}

	public List<String> getMedicalCenterGeographicAreas() {
		if (medicalCenterGeographicAreas == null)
			medicalCenterGeographicAreas = new ArrayList<String>();
		return medicalCenterGeographicAreas;
	}

	public void setMedicalCenterGeographicAreas(
			List<String> medicalCenterGeographicAreas) {
		this.medicalCenterGeographicAreas = medicalCenterGeographicAreas;
	}

	public Claim getClaim() {
		return claim;
	}

	public void setClaim(Claim claim) {
		this.claim = claim;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String getClaimBilledAmountJETEligibility() {
		return claimBilledAmountJETEligibility;
	}

	public void setClaimBilledAmountJETEligibility(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String claimBilledAmountJETEligibility) {
		this.claimBilledAmountJETEligibility = claimBilledAmountJETEligibility;

	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.JETStatus") String getClaimJETStatus() {
		return claimJETStatus;
	}

	public void setClaimJETStatus(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.JETStatus") String claimJETStatus) {
		this.claimJETStatus = claimJETStatus;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String getClaimJETEligible() {
		return claimJETEligible;
	}

	public void setClaimJETEligible(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String claimJETEligible) {
		this.claimJETEligible = claimJETEligible;

	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String getClaimProductJETEligible() {
		return claimProductJETEligible;
	}

	public void setClaimProductJETEligible(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String claimProductJETEligible) {
		this.claimProductJETEligible = claimProductJETEligible;
	}

	public List<Diagnosis> getAcuteDiagnosisList() {
		if (acuteDiagnosisList == null)
			acuteDiagnosisList = new ArrayList<Diagnosis>();
		return acuteDiagnosisList;
	}

	public List<Diagnosis> getAcuteOnlyDiagnosisList() {
		if (acuteOnlyDiagnosisList == null)
			acuteOnlyDiagnosisList = new ArrayList<Diagnosis>();
		return acuteOnlyDiagnosisList;
	}

	public List<Diagnosis> getChronicDiagnosisList() {
		if (chronicDiagnosisList == null)
			chronicDiagnosisList = new ArrayList<Diagnosis>();
		return chronicDiagnosisList;
	}

	public List<Diagnosis> getCatastrophicDiagnosisList() {
		if (catastrophicDiagnosisList == null)
			catastrophicDiagnosisList = new ArrayList<Diagnosis>();
		return catastrophicDiagnosisList;
	}

	public List<Treatment> getSeverity1SurgicalCodes() {
		if (severity1SurgicalCodes == null)
			severity1SurgicalCodes = new ArrayList<Treatment>();
		return severity1SurgicalCodes;
	}

	public List<Treatment> getSeverity2SurgicalCodes() {
		if (severity2SurgicalCodes == null)
			severity2SurgicalCodes = new ArrayList<Treatment>();
		return severity2SurgicalCodes;
	}

	public List<Treatment> getSeverity3SurgicalCodes() {
		if (severity3SurgicalCodes == null)
			severity3SurgicalCodes = new ArrayList<Treatment>();
		return severity3SurgicalCodes;
	}

	public List<Treatment> getSeverity4SurgicalCodes() {
		if (severity4SurgicalCodes == null)
			severity4SurgicalCodes = new ArrayList<Treatment>();
		return severity4SurgicalCodes;
	}

	public List<Treatment> getSeverity5SurgicalCodes() {
		if (severity5SurgicalCodes == null)
			severity5SurgicalCodes = new ArrayList<Treatment>();
		return severity5SurgicalCodes;
	}

	public List<Treatment> getSeverity6SurgicalCodes() {
		if (severity6SurgicalCodes == null)
			severity6SurgicalCodes = new ArrayList<Treatment>();
		return severity6SurgicalCodes;
	}

	public List<Treatment> getSeverity7SurgicalCodes() {
		if (severity7SurgicalCodes == null)
			severity7SurgicalCodes = new ArrayList<Treatment>();
		return severity7SurgicalCodes;
	}

	public int getClaimSurgicalEventCount() {
		int count = 0;
		if (null != this.getSeverity1SurgicalCodes()
				&& this.getSeverity1SurgicalCodes().size() > 0)
			count += this.getSeverity1SurgicalCodes().size();
		if (null != this.getSeverity2SurgicalCodes()
				&& this.getSeverity2SurgicalCodes().size() > 0)
			count += this.getSeverity2SurgicalCodes().size();
		if (null != this.getSeverity3SurgicalCodes()
				&& this.getSeverity3SurgicalCodes().size() > 0)
			count += this.getSeverity3SurgicalCodes().size();
		if (null != this.getSeverity4SurgicalCodes()
				&& this.getSeverity4SurgicalCodes().size() > 0)
			count += this.getSeverity4SurgicalCodes().size();
		if (null != this.getSeverity5SurgicalCodes()
				&& this.getSeverity5SurgicalCodes().size() > 0)
			count += this.getSeverity5SurgicalCodes().size();
		if (null != this.getSeverity6SurgicalCodes()
				&& this.getSeverity6SurgicalCodes().size() > 0)
			count += this.getSeverity6SurgicalCodes().size();
		if (null != this.getSeverity7SurgicalCodes()
				&& this.getSeverity7SurgicalCodes().size() > 0)
			count += this.getSeverity7SurgicalCodes().size();
		return count;
	}

	public int getClaimHighestSeverity() {
		if (null != this.getSeverity7SurgicalCodes()
				&& this.getSeverity7SurgicalCodes().size() > 0) {
			claimHighestSeverity = 7;
			return claimHighestSeverity;
		} else if (null != this.getSeverity6SurgicalCodes()
				&& this.getSeverity6SurgicalCodes().size() > 0) {
			claimHighestSeverity = 6;
			return claimHighestSeverity;
		} else if (null != this.getSeverity5SurgicalCodes()
				&& this.getSeverity5SurgicalCodes().size() > 0) {
			claimHighestSeverity = 5;
			return claimHighestSeverity;
		} else if (null != this.getSeverity4SurgicalCodes()
				&& this.getSeverity3SurgicalCodes().size() > 0) {
			claimHighestSeverity = 4;
			return claimHighestSeverity;
		} else if (null != this.getSeverity3SurgicalCodes()
				&& this.getSeverity3SurgicalCodes().size() > 0) {
			claimHighestSeverity = 3;
			return claimHighestSeverity;
		} else if (null != this.getSeverity2SurgicalCodes()
				&& this.getSeverity2SurgicalCodes().size() > 0) {
			claimHighestSeverity = 2;
			return claimHighestSeverity;
		} else if (null != this.getSeverity1SurgicalCodes()
				&& this.getSeverity1SurgicalCodes().size() > 0) {
			claimHighestSeverity = 1;
			return claimHighestSeverity;
		}

		return claimHighestSeverity;
	}

	/**
	 * @return the claimJETValidityAssessment
	 */
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ValidityStatus") String getClaimJETValidityAssessment() {
		return claimJETValidityAssessment;
	}

	/**
	 * @param claimJETValidityAssessment
	 *            the claimJETValidityAssessment to set
	 */
	public void setClaimJETValidityAssessment(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ValidityStatus") String claimJETValidityAssessment) {
		this.claimJETValidityAssessment = claimJETValidityAssessment;
	}

	/**
	 * @return the claimMedicalWastageAssessment
	 */
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.WastageAssessment") String getClaimMedicalWastageAssessment() {
		return claimMedicalWastageAssessment;
	}

	/**
	 * @param claimMedicalWastageAssessment
	 *            the claimMedicalWastageAssessment to set
	 */
	public void setClaimMedicalWastageAssessment(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.WastageAssessment") String claimMedicalWastageAssessment) {
		this.claimMedicalWastageAssessment = claimMedicalWastageAssessment;
	}

	/**
	 * @return the earlyClaimStatus
	 */
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.EarlyClaimStatus") String getEarlyClaimStatus() {
		return earlyClaimStatus;
	}

	/**
	 * @param earlyClaimStatus
	 *            the earlyClaimStatus to set
	 */
	public void setEarlyClaimStatus(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.EarlyClaimStatus") String earlyClaimStatus) {
		this.earlyClaimStatus = earlyClaimStatus;
	}

	/**
	 * @return the preExistingConditionStatus
	 */
	public String getPreExistingConditionStatus() {
		return preExistingConditionStatus;
	}

	/**
	 * @param preExistingConditionStatus
	 *            the preExistingConditionStatus to set
	 */
	public void setPreExistingConditionStatus(String preExistingConditionStatus) {
		this.preExistingConditionStatus = preExistingConditionStatus;
	}

	/**
	 * @return the claimPolicyandBenefitEligibilityStatus
	 */
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String getClaimPolicyandBenefitEligibilityStatus() {
		return claimPolicyandBenefitEligibilityStatus;
	}

	/**
	 * @param claimPolicyandBenefitEligibilityStatus
	 *            the claimPolicyandBenefitEligibilityStatus to set
	 */
	public void setClaimPolicyandBenefitEligibilityStatus(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String claimPolicyandBenefitEligibilityStatus) {
		this.claimPolicyandBenefitEligibilityStatus = claimPolicyandBenefitEligibilityStatus;
	}

	/**
	 * @return the claimWithin120daysofPolicyInForceDate
	 */
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.WithIn120daysOfPolicyInForceDate") String getClaimWithin120daysofPolicyInForceDate() {
		return claimWithin120daysofPolicyInForceDate;
	}

	/**
	 * @param claimWithin120daysofPolicyInForceDate
	 *            the claimWithin120daysofPolicyInForceDate to set
	 */
	public void setClaimWithin120daysofPolicyInForceDate(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.WithIn120daysOfPolicyInForceDate") String claimWithin120daysofPolicyInForceDate) {
		this.claimWithin120daysofPolicyInForceDate = claimWithin120daysofPolicyInForceDate;
	}

	/**
	 * @return the claimWithin12monthsofPolicyInForceDate
	 */
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.WithIn12monthsOfPolicyInForceDate") String getClaimWithin12monthsofPolicyInForceDate() {
		return claimWithin12monthsofPolicyInForceDate;
	}

	/**
	 * @param claimWithin12monthsofPolicyInForceDate
	 *            the claimWithin12monthsofPolicyInForceDate to set
	 */
	public void setClaimWithin12monthsofPolicyInForceDate(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.WithIn12monthsOfPolicyInForceDate") String claimWithin12monthsofPolicyInForceDate) {
		this.claimWithin12monthsofPolicyInForceDate = claimWithin12monthsofPolicyInForceDate;
	}

	/**
	 * @return the claimRejectionDetails
	 */
	public List<ClaimRejectionDetail> getClaimRejectionDetails() {
		if (null == claimRejectionDetails)
			claimRejectionDetails = new ArrayList<ClaimRejectionDetail>();
		return claimRejectionDetails;
	}

	/**
	 * @param claimRejectionDetails
	 *            the claimRejectionDetails to set
	 */
	public void setClaimRejectionDetails(
			List<ClaimRejectionDetail> claimRejectionDetails) {
		this.claimRejectionDetails = claimRejectionDetails;
	}

	/**
	 * @return the policyIndividualExclusionStatus
	 */
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus") String getPolicyIndividualExclusionStatus() {
		return policyIndividualExclusionStatus;
	}

	/**
	 * @param policyIndividualExclusionStatus
	 *            the policyIndividualExclusionStatus to set
	 */
	public void setPolicyIndividualExclusionStatus(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus") String policyIndividualExclusionStatus) {
		this.policyIndividualExclusionStatus = policyIndividualExclusionStatus;
	}

	/**
	 * @return the policyProductSpecificExclusionStatus
	 */
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus") String getPolicyProductSpecificExclusionStatus() {
		return policyProductSpecificExclusionStatus;
	}

	/**
	 * @param policyProductSpecificExclusionStatus
	 *            the policyProductSpecificExclusionStatus to set
	 */
	public void setPolicyProductSpecificExclusionStatus(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus") String policyProductSpecificExclusionStatus) {
		this.policyProductSpecificExclusionStatus = policyProductSpecificExclusionStatus;
	}

	/**
	 * @return the policyAllProductExclusionStatus
	 */
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus") String getPolicyAllProductExclusionStatus() {
		return policyAllProductExclusionStatus;
	}

	/**
	 * @param policyAllProductExclusionStatus
	 *            the policyAllProductExclusionStatus to set
	 */
	public void setPolicyAllProductExclusionStatus(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus") String policyAllProductExclusionStatus) {
		this.policyAllProductExclusionStatus = policyAllProductExclusionStatus;
	}

	/**
	 * @return the policyGeneralExclusionStatus
	 */
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus") String getPolicyGeneralExclusionStatus() {
		return policyGeneralExclusionStatus;
	}

	/**
	 * @param policyGeneralExclusionStatus
	 *            the policyGeneralExclusionStatus to set
	 */
	public void setPolicyGeneralExclusionStatus(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus") String policyGeneralExclusionStatus) {
		this.policyGeneralExclusionStatus = policyGeneralExclusionStatus;
	}

	/**
	 * @return the claimExclusionStatus
	 */
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus") String getClaimExclusionStatus() {
		return claimExclusionStatus;
	}

	/**
	 * @param claimExclusionStatus
	 *            the claimExclusionStatus to set
	 */
	public void setClaimExclusionStatus(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ExclusionStatus") String claimExclusionStatus) {
		this.claimExclusionStatus = claimExclusionStatus;
	}

	/**
	 * @return the diagnosisJETEligibility
	 */
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String getDiagnosisJETEligibility() {
		return diagnosisJETEligibility;
	}

	/**
	 * @param diagnosisJETEligibility
	 *            the diagnosisJETEligibility to set
	 */
	public void setDiagnosisJETEligibility(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String diagnosisJETEligibility) {
		this.diagnosisJETEligibility = diagnosisJETEligibility;
	}

	/**
	 * @return the policyInForceDate
	 */
	public Date getPolicyInForceDate() {
		return policyInForceDate;
	}

	/**
	 * @param policyInForceDate
	 *            the policyInForceDate to set
	 */
	public void setPolicyInForceDate(Date policyInForceDate) {
		this.policyInForceDate = policyInForceDate;
	}

	/**
	 * @param claimHighestSeverity
	 *            the claimHighestSeverity to set
	 */
	public void setClaimHighestSeverity(int claimHighestSeverity) {
		this.claimHighestSeverity = claimHighestSeverity;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.Assessment") String getTreatmentLengthofStayAssessment() {
		return treatmentLengthofStayAssessment;
	}

	public void setTreatmentLengthofStayAssessment(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.Assessment") String treatmentLengthofStayAssessment) {
		this.treatmentLengthofStayAssessment = treatmentLengthofStayAssessment;
	}

	public int getTreatmentLOS50thPercentileMax() {
		return treatmentLOS50thPercentileMax;
	}

	public void setTreatmentLOS50thPercentileMax(
			int treatmentLOS50thPercentileMax) {
		this.treatmentLOS50thPercentileMax = treatmentLOS50thPercentileMax;
	}

	public int getTreatmentLOS75thPercentileMax() {
		return treatmentLOS75thPercentileMax;
	}

	public void setTreatmentLOS75thPercentileMax(
			int treatmentLOS75thPercentileMax) {
		this.treatmentLOS75thPercentileMax = treatmentLOS75thPercentileMax;
	}

	public int getTreatmentLOS90thPercentileMax() {
		return treatmentLOS90thPercentileMax;
	}

	public void setTreatmentLOS90thPercentileMax(
			int treatmentLOS90thPercentileMax) {
		this.treatmentLOS90thPercentileMax = treatmentLOS90thPercentileMax;
	}

	public String getSurgicalCodeAssessment() {
		return surgicalCodeAssessment;
	}

	public void setSurgicalCodeAssessment(String surgicalCodeAssessment) {
		this.surgicalCodeAssessment = surgicalCodeAssessment;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.SurgeryAppropriateness") String getSurgeryAppropriateness() {
		return surgeryAppropriateness;
	}

	public void setSurgeryAppropriateness(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.SurgeryAppropriateness") String surgeryAppropriateness) {
		this.surgeryAppropriateness = surgeryAppropriateness;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.Assessment") String getTreatmentLOSAssessment() {
		return treatmentLOSAssessment;
	}

	public void setTreatmentLOSAssessment(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.Assessment") String treatmentLOSAssessment) {
		this.treatmentLOSAssessment = treatmentLOSAssessment;
	}

	public String getTreatmentHospitalizationAssessment() {
		return treatmentHospitalizationAssessment;
	}

	public void setTreatmentHospitalizationAssessment(
			String treatmentHospitalizationAssessment) {
		this.treatmentHospitalizationAssessment = treatmentHospitalizationAssessment;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String getClaimComplexityAssessment() {
		return claimComplexityAssessment;
	}

	public void setClaimComplexityAssessment(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String claimComplexityAssessment) {
		this.claimComplexityAssessment = claimComplexityAssessment;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String getEventComplexityAssessment() {
		return eventComplexityAssessment;
	}

	public void setEventComplexityAssessment(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String eventComplexityAssessment) {
		this.eventComplexityAssessment = eventComplexityAssessment;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String getDiagnosisComplexityAssessment() {
		return diagnosisComplexityAssessment;
	}

	public void setDiagnosisComplexityAssessment(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String diagnosisComplexityAssessment) {
		this.diagnosisComplexityAssessment = diagnosisComplexityAssessment;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String getDiagnosisComboComplexityAssessment() {
		return diagnosisComboComplexityAssessment;
	}

	public void setDiagnosisComboComplexityAssessment(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String diagnosisComboComplexityAssessment) {
		this.diagnosisComboComplexityAssessment = diagnosisComboComplexityAssessment;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String getClaimHistoryComplexityAssessment() {
		return claimHistoryComplexityAssessment;
	}

	public void setClaimHistoryComplexityAssessment(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String claimHistoryComplexityAssessment) {
		this.claimHistoryComplexityAssessment = claimHistoryComplexityAssessment;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String getTreatmentComplexityAssessment() {
		return treatmentComplexityAssessment;
	}

	public void setTreatmentComplexityAssessment(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.ComplexityStatus") String treatmentComplexityAssessment) {
		this.treatmentComplexityAssessment = treatmentComplexityAssessment;
	}

	public List<Product> getEligibleProductList() {
		if (eligibleProductList == null)
			eligibleProductList = new ArrayList<Product>();
		return eligibleProductList;
	}

	public List<Product> getIneligibleProductList() {
		if (ineligibleProductList == null)
			ineligibleProductList = new ArrayList<Product>();
		return ineligibleProductList;
	}

	public int getNoOfTotalTreatmentCodes() {
		return noOfTotalTreatmentCodes;
	}

	public void setNoOfTotalTreatmentCodes(int noOfTotalTreatmentCodes) {
		this.noOfTotalTreatmentCodes = noOfTotalTreatmentCodes;
	}

	public List<String> getSurgeryAssessmentList() {
		if (surgeryAssessmentList == null)
			surgeryAssessmentList = new ArrayList<String>();
		return surgeryAssessmentList;
	}

	public List<LOSPersentile> getTreatmentLOSAssessmentList() {
		if (treatmentLOSAssessmentList == null)
			treatmentLOSAssessmentList = new ArrayList<LOSPersentile>();
		return treatmentLOSAssessmentList;
	}

	public void setTreatmentLOSAssessmentList(
			List<LOSPersentile> treatmentLOSAssessmentList) {
		this.treatmentLOSAssessmentList = treatmentLOSAssessmentList;
	}

	public List<String> getSurgeryList() {
		if (surgeryList == null)
			surgeryList = new ArrayList<String>();
		return surgeryList;
	}
	
}
