package com.phkl.businessrules.claims.jetStatus;

import java.util.ArrayList;
import java.util.List;

import com.phkl.businessrules.claims.common.Request;
import com.phkl.businessrules.claims.domain.Claim;
import com.phkl.businessrules.claims.domain.Policy;

public class DetermineClaimJetStatusRequest extends Request{
	private Claim currentClaim = new Claim();
	private Policy policy = new Policy();
	private List<Claim> previousClaims =  new ArrayList<Claim>();

	public Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Policy policy) {
		this.policy = policy;
	}

	public void setCurrentClaim(Claim currentClaim) {
		this.currentClaim = currentClaim;
	}

	public Claim getCurrentClaim() {
		return currentClaim;
	}

	public List<Claim> getPreviousClaims() {
		if (previousClaims == null)
			previousClaims = new ArrayList<Claim>();
		return previousClaims;
	}

	public void setPreviousClaims(List<Claim> previousClaims) {
		this.previousClaims = previousClaims;
	}

}
