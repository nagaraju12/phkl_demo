package com.phkl.businessrules.claims.domain;

public class ClaimReceiptItem {
	private String receiptItem = "";
	private double receiptItemCode = 0.0;
	
	public String getReceiptItem() {
		return receiptItem;
	}
	public void setReceiptItem(String receiptItem) {
		this.receiptItem = receiptItem;
	}
	public double getReceiptItemCode() {
		return receiptItemCode;
	}
	public void setReceiptItemCode(double receiptItemCode) {
		this.receiptItemCode = receiptItemCode;
	}
	
}
