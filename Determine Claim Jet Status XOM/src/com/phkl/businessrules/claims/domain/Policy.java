package com.phkl.businessrules.claims.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Policy {
    private String policyID;

    private PolicyHolder policyHolder;
    private LifeAssured lifeAssured;
    private Date riskCommencementDate;
    // Can have a collection of products
    private List<Product> products;
    private List<String> coveredDiagnosisCodesList;
    private List<String> excludedDiagnosisCodes;

    private Date policyLastRevivalDate;
    private Date policyEffectiveDate;
    private Date policyInForceDate;
    private String policyCurrency;

    public String getPolicyCurrency() {
	return policyCurrency;
    }

    public void setPolicyCurrency(String policyCurrency) {
	this.policyCurrency = policyCurrency;
    }

    public String getPolicyID() {
	return policyID;
    }

    public void setPolicyID(String policyID) {
	this.policyID = policyID;
    }

    /**
     * @return the policyHolder
     */
    public PolicyHolder getPolicyHolder() {
	return policyHolder;
    }

    /**
     * @param policyHolder
     *            the policyHolder to set
     */
    public void setPolicyHolder(PolicyHolder policyHolder) {
	this.policyHolder = policyHolder;
    }

    /**
     * @return the lifeAssured
     */
    public LifeAssured getLifeAssured() {
	return lifeAssured;
    }

    /**
     * @param lifeAssured
     *            the lifeAssured to set
     */
    public void setLifeAssured(LifeAssured lifeAssured) {
	this.lifeAssured = lifeAssured;
    }

    /**
     * @return the riskCommencementDate
     */
    public Date getRiskCommencementDate() {
	return riskCommencementDate;
    }

    /**
     * @param riskCommencementDate
     *            the riskCommencementDate to set
     */
    public void setRiskCommencementDate(Date riskCommencementDate) {
	this.riskCommencementDate = riskCommencementDate;
    }

    /**
     * @return the products
     */
    public List<Product> getProducts() {
	return products;
    }

    /**
     * @param products
     *            the products to set
     */
    public void setProducts(List<Product> products) {
	this.products = products;
    }

    /**
     * @return the coveredDiagnosisCodesList
     */
    public List<String> getCoveredDiagnosisCodesList() {
	return coveredDiagnosisCodesList;
    }

    /**
     * @param coveredDiagnosisCodesList
     *            the coveredDiagnosisCodesList to set
     */
    public void setCoveredDiagnosisCodesList(
	    List<String> coveredDiagnosisCodesList) {
	this.coveredDiagnosisCodesList = coveredDiagnosisCodesList;
    }

    /**
     * @return the policyLastRevivalDate
     */
    public Date getPolicyLastRevivalDate() {
	return policyLastRevivalDate;
    }

    /**
     * @param policyLastRevivalDate
     *            the policyLastRevivalDate to set
     */
    public void setPolicyLastRevivalDate(Date policyLastRevivalDate) {
	this.policyLastRevivalDate = policyLastRevivalDate;
    }

    /**
     * @return the policyEffectiveDate
     */
    public Date getPolicyEffectiveDate() {
	return policyEffectiveDate;
    }

    /**
     * @param policyEffectiveDate
     *            the policyEffectiveDate to set
     */
    public void setPolicyEffectiveDate(Date policyEffectiveDate) {
	this.policyEffectiveDate = policyEffectiveDate;
    }

    /**
     * @return the policyInForceDate
     */
    public Date getPolicyInForceDate() {
	return policyInForceDate;
    }

    /**
     * @param policyInForceDate
     *            the policyInForceDate to set
     */
    public void setPolicyInForceDate(Date policyInForceDate) {
	this.policyInForceDate = policyInForceDate;
    }

    public List<String> getExcludedDiagnosisCodes() {
	if (null == excludedDiagnosisCodes)
	    excludedDiagnosisCodes = new ArrayList<String>();
	return excludedDiagnosisCodes;
    }

    public void setExcludedDiagnosisCodes(List<String> excludedDiagnosisCodes) {
	this.excludedDiagnosisCodes = excludedDiagnosisCodes;
    }
}
