package com.phkl.businessrules.claims.domain;

import ilog.rules.bom.annotations.BusinessName;
import ilog.rules.bom.annotations.BusinessType;


public class Product {
	@BusinessName("com.phkl.businessrules.claims.virtualdomain.ProductCode")
	private String code;
	private String name;
	private String description;
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus")
	private String jetEligibility;
	public 	@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus")String getJetEligibility() {
		return jetEligibility;
	}

	public void setJetEligibility(	@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus")String jetEligibility) {
		this.jetEligibility = jetEligibility;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.ProductCode")String getCode() {
		return code;
	}

	public void setCode(@BusinessType("com.phkl.businessrules.claims.virtualdomain.ProductCode")String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
