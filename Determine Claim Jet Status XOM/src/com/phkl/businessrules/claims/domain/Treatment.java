package com.phkl.businessrules.claims.domain;

import java.util.Date;

public class Treatment {

	private String treatmentCode;
	private String description;
	private String type;
	private Date treatmentStartDate;
	private Date treatmentEndDate;
	private int treatmentDuration;
	private double treatmentCost;
	private String currency;

	// private MedicalCenter medicalCenter;

	public double getTreatmentCost() {
		return treatmentCost;
	}

	public void setTreatmentCost(double treatmentCost) {
		this.treatmentCost = treatmentCost;
	}

	/**
	 * @return the treatmentCode
	 */
	public String getTreatmentCode() {
		return treatmentCode;
	}

	/**
	 * @param treatmentCode
	 *            the treatmentCode to set
	 */
	public void setTreatmentCode(String treatmentCode) {
		this.treatmentCode = treatmentCode;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the treastmentStartDate
	 */
	public Date getTreatmentStartDate() {
		return treatmentStartDate;
	}

	/**
	 * @param treastmentStartDate
	 *            the treastmentStartDate to set
	 */
	public void setTreatmentStartDate(Date treatmentStartDate) {
		this.treatmentStartDate = treatmentStartDate;
	}

	/**
	 * @return the treatmentEndDate
	 */
	public Date getTreatmentEndDate() {
		return treatmentEndDate;
	}

	/**
	 * @param treatmentEndDate
	 *            the treatmentEndDate to set
	 */
	public void setTreatmentEndDate(Date treatmentEndDate) {
		this.treatmentEndDate = treatmentEndDate;
	}

	public int getTreatmentDuration() {
		return treatmentDuration;
	}

	public void setTreatmentDuration(int treatmentDuration) {
		this.treatmentDuration = treatmentDuration;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
