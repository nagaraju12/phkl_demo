package com.phkl.businessrules.claims.domain;

import ilog.rules.bom.annotations.BusinessType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClaimEvent {

	private Date eventStartDate = new Date();
	private Date eventEndDate = new Date();
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.EventType")
	private String eventType = "";
	private String eventId = "";
	private List<Diagnosis> diagnosisList = new ArrayList<Diagnosis>();
	private MedicalCenter medicalCenter = new MedicalCenter();
	private List<Treatment> treatments = new ArrayList<Treatment>();

	public List<Diagnosis> getDiagnosisList() {
		if (diagnosisList == null)
			diagnosisList = new ArrayList<Diagnosis>();
		return diagnosisList;
	}

	public void setDiagnosisList(List<Diagnosis> diagnosisList) {
		this.diagnosisList = diagnosisList;
	}

	public Date getEventStartDate() {
		return eventStartDate;
	}

	public void setEventStartDate(Date eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public Date getEventEndDate() {
		return eventEndDate;
	}

	public void setEventEndDate(Date eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.EventType") String getEventType() {
		return eventType;
	}

	public void setEventType(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.EventType") String eventType) {
		this.eventType = eventType;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public MedicalCenter getMedicalCenter() {
		return medicalCenter;
	}

	public void setMedicalCenter(MedicalCenter medicalCenter) {
		this.medicalCenter = medicalCenter;
	}

	public List<Treatment> getTreatments() {
		if (treatments == null)
			treatments = new ArrayList<Treatment>();
		return treatments;
	}

	public void setTreatments(List<Treatment> treatments) {
		this.treatments = treatments;
	}

}
