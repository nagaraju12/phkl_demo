package com.phkl.businessrules.claims.domain;

import ilog.rules.bom.annotations.BusinessType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Claim {
	private String claimID = "";
	private String policyID = "";
	private String type = "";
	private String deathclaimIndicator = "";
	private String disabilityIndicator = "";
	private String criticalIllnessIndicator = "";
	private String medicalClaimsIndicator = "";
	private String accidentIndicator = "";
	private double medicalWastageScore = 0.0;
	private double hospitalizationNecessaryScore = 0.0;
	private double preExistingConditiionScore = 0.0;

	private double billedAmountValue = 0.0;
	private String billAmountCurrency = "";
	private Date submissionDate = new Date();;
	private Date approvedDate = new Date();
	//private List<Treatment> treatments = new ArrayList<Treatment>();
	// private List<String> claimPreExistingConditionStatusList;
	// private List<String> claimRCSRedFlagCategoryList;
	private List<String> claimRedFlagCategoryList = new ArrayList<String>();
	private List<String> benefitCodes = new ArrayList<String>();

	private List<ClaimEvent> claimEvents = new ArrayList<ClaimEvent>();
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.UnstructuredDataFlag")
	private String unstructuredDataFlag = "";
	
	
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus")
	private String policyAndBenefitElibigilityStatus = "";

	private List<Benefit> benefitTypeList = new ArrayList<Benefit>();
	
	
	
	 public List<Benefit> getbenefitTypeList() {
	if (benefitTypeList == null)
		benefitTypeList = new ArrayList<Benefit>();
	return benefitTypeList;
}
public void setbenefitTypeList(List<Benefit> benefitTypeList) {
	this.benefitTypeList = benefitTypeList;
}  
	
	private Date claimHospitalInDate= new Date();
	private Date claimHospitalOutDate= new Date();
	
	public Date getClaimHospitalInDate() {
		return claimHospitalInDate;
	}

	public void setClaimHospitalInDate(Date claimHospitalInDate) {
		this.claimHospitalInDate = claimHospitalInDate;
	}

	public Date getClaimHospitalOutDate() {
		return claimHospitalOutDate;
	}

	public void setClaimHospitalOutDate(Date claimHospitalOutDate) {
		this.claimHospitalOutDate = claimHospitalOutDate;
	}

	
	
	public String getDeathclaimIndicator() {
		return deathclaimIndicator;
	}

	public void setDeathclaimIndicator(String deathclaimIndicator) {
		this.deathclaimIndicator = deathclaimIndicator;
	}

	public String getDisabilityIndicator() {
		return disabilityIndicator;
	}

	public void setDisabilityIndicator(String disabilityIndicator) {
		this.disabilityIndicator = disabilityIndicator;
	}

	public String getCriticalIllnessIndicator() {
		return criticalIllnessIndicator;
	}

	public void setCriticalIllnessIndicator(String criticalIllnessIndicator) {
		this.criticalIllnessIndicator = criticalIllnessIndicator;
	}

	public String getMedicalClaimsIndicator() {
		return medicalClaimsIndicator;
	}

	public void setMedicalClaimsIndicator(String medicalClaimsIndicator) {
		this.medicalClaimsIndicator = medicalClaimsIndicator;
	}

	public String getAccidentIndicator() {
		return accidentIndicator;
	}

	public void setAccidentIndicator(String accidentIndicator) {
		this.accidentIndicator = accidentIndicator;
	}

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String getPolicyAndBenefitElibigilityStatus() {
		return policyAndBenefitElibigilityStatus;
	}

	public void setPolicyAndBenefitElibigilityStatus(@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String policyAndBenefitElibigilityStatus) {
		this.policyAndBenefitElibigilityStatus = policyAndBenefitElibigilityStatus;
	}

	public List<String> getBenefitCodes() {
		if (benefitCodes == null)
			return new ArrayList<String>();
		return benefitCodes;
	}

	public void setBenefitCodes(List<String> benefitCodes) {
		this.benefitCodes = benefitCodes;
	}

	public double getPreExistingConditiionScore() {
		return preExistingConditiionScore;
	}

	public void setPreExistingConditiionScore(double preExistingConditiionScore) {
		this.preExistingConditiionScore = preExistingConditiionScore;
	}

	//
	// public Amount getBilledAmount() {
	// return billedAmount;
	// }
	//
	// public void setBilledAmount(Amount billedAmount) {
	// this.billedAmount = billedAmount;
	// }

	// private List<Product> claimAgainstproductList;

	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.UnstructuredDataFlag") String getUnstructuredDataFlag() {
		return unstructuredDataFlag;
	}

	public void setUnstructuredDataFlag(@BusinessType("com.phkl.businessrules.claims.virtualdomain.UnstructuredDataFlag") String unstructuredDataFlag) {
		this.unstructuredDataFlag = unstructuredDataFlag;
	}

	public double getMedicalWastageScore() {
		return medicalWastageScore;
	}

	public void setMedicalWastageScore(double medicalWastageScore) {
		this.medicalWastageScore = medicalWastageScore;
	}

	public double getHospitalizationNecessaryScore() {
		return hospitalizationNecessaryScore;
	}

	public void setHospitalizationNecessaryScore(double hospitalizationNecessaryScore) {
		this.hospitalizationNecessaryScore = hospitalizationNecessaryScore;
	}

	public List<ClaimEvent> getClaimEvents() {
		if (claimEvents == null)
			claimEvents = new ArrayList<ClaimEvent>();

		return claimEvents;
	}

	public void setClaimEvents(List<ClaimEvent> claimEvents) {
		this.claimEvents = claimEvents;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	// public List<Product> getProductList() {
	// if (productList == null)
	// productList = new ArrayList<Product>();
	// return productList;
	// }
	//
	// public void setProductList(List<Product> productList) {
	// this.productList = productList;
	// }

	public Date getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	// public static int getProductCountByEligibility(String productEligibility,
	// Claim claim) {
	// int count = 0;
	// if (null != claim.getProductList()) {
	// if (claim.getProductList().size() == 0)
	// return 0;
	// if (claim.getProductList().size() > 0) {
	// for (int i = 0; i < claim.getProductList().size(); i++) {
	// if (null != claim.getProductList().get(i)
	// .getJetEligibility()
	// && claim.getProductList().get(i)
	// .getJetEligibility()
	// .equals(productEligibility))
	// count++;
	// }
	// }
	// }
	// return count;
	// }

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	// public List<String> getClaimPreExistingConditionStatusList() {
	// if (claimPreExistingConditionStatusList == null)
	// claimPreExistingConditionStatusList = new ArrayList<String>();
	// return claimPreExistingConditionStatusList;
	// }
	//
	// public void setClaimPreExistingConditionStatusList(
	// List<String> claimPreExistingConditionStatusList) {
	// this.claimPreExistingConditionStatusList =
	// claimPreExistingConditionStatusList;
	// }

	/**
	 * @return the treatmentDetail //
	 */
	// public TreatmentDetail getTreatmentDetail() {
	// return treatmentDetail;
	// }
	//
	// /**
	// * @param treatmentDetail
	// * the treatmentDetail to set
	// */
	// public void setTreatmentDetail(TreatmentDetail treatmentDetail) {
	// this.treatmentDetail = treatmentDetail;
	// }

	/**
	 * @return the claimID
	 */
	public String getClaimID() {
		return claimID;
	}

	/**
	 * @param claimID
	 *            the claimID to set
	 */
	public void setClaimID(String claimID) {
		this.claimID = claimID;
	}

	/**
	 * @return the policyID
	 */
	public String getPolicyID() {
		return policyID;
	}

	/**
	 * @param policyID
	 *            the policyID to set
	 */
	public void setPolicyID(String policyID) {
		this.policyID = policyID;
	}

	/**
	 * @return the diagnosisDetail
	 */
	// public DiagnosisDetail getDiagnosisDetail() {
	// return diagnosisDetail;
	// }
	//
	// /**
	// * @param diagnosisDetail
	// * the diagnosisDetail to set
	// */
	// public void setDiagnosisDetail(DiagnosisDetail diagnosisDetail) {
	// this.diagnosisDetail = diagnosisDetail;
	// }

	/**
	 * @return the claimRCSRedFlagCategoryList
	 */
	// public List<String> getClaimRCSRedFlagCategoryList() {
	// return claimRCSRedFlagCategoryList;
	// }
	//
	// /**
	// * @param claimRCSRedFlagCategoryList the claimRCSRedFlagCategoryList to
	// set
	// */
	// public void setClaimRCSRedFlagCategoryList(
	// List<String> claimRCSRedFlagCategoryList) {
	// this.claimRCSRedFlagCategoryList = claimRCSRedFlagCategoryList;
	// }
	//
	// /**
	// * @return the claimPRULifeRedFlagCategoryList
	// */
	// public List<String> getClaimPRULifeRedFlagCategoryList() {
	// return claimPRULifeRedFlagCategoryList;
	// }
	//
	// /**
	// * @param claimPRULifeRedFlagCategoryList the
	// claimPRULifeRedFlagCategoryList to set
	// */
	// public void setClaimPRULifeRedFlagCategoryList(
	// List<String> claimPRULifeRedFlagCategoryList) {
	// this.claimPRULifeRedFlagCategoryList = claimPRULifeRedFlagCategoryList;
	// }

	/**
	 * @return the benefitTypes
	 */
	// public List<String> getBenefitTypes() {
	// return benefitTypes;
	// }
	//
	// /**
	// * @param benefitTypes
	// * the benefitTypes to set
	// */
	// public void setBenefitTypes(List<String> benefitTypes) {
	// this.benefitTypes = benefitTypes;
	// }

	public List<String> getClaimRedFlagCategoryList() {
		if (claimRedFlagCategoryList == null)
			claimRedFlagCategoryList = new ArrayList<String>();
		return claimRedFlagCategoryList;
	}

	public void setClaimRedFlagCategoryList(List<String> claimRedFlagCategoryList) {
		this.claimRedFlagCategoryList = claimRedFlagCategoryList;
	}

	public String getBillAmountCurrency() {
		return billAmountCurrency;
	}

	public void setBillAmountCurrency(String billAmountCurrency) {
		this.billAmountCurrency = billAmountCurrency;
	}
//
//	public List<Treatment> getTreatments() {
//		return treatments;
//	}
//
//	public void setTreatments(List<Treatment> treatments) {
//		this.treatments = treatments;
//	}

	public double getBilledAmountValue() {
		return billedAmountValue;
	}

	public void setBilledAmountValue(double billedAmountValue) {
		this.billedAmountValue = billedAmountValue;
	}

}
