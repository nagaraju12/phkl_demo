package com.phkl.businessrules.claims.domain;


public class MedicalCenter {

	private String code = "";
	private String name = "";
	private String geographicArea = "";

	public String getGeographicArea() {
		return geographicArea;
	}

	public void setGeographicArea(String geographicArea) {
		this.geographicArea = geographicArea;
	}

	private String medicalCenterType;

	public String getMedicalCenterType() {
		return medicalCenterType;
	}

	public void setMedicalCenterType(String medicalCenterType) {
		this.medicalCenterType = medicalCenterType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
