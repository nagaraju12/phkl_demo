package com.phkl.businessrules.claims.domain;

import ilog.rules.bom.annotations.BusinessType;

import java.util.Date;

public class Diagnosis {
	private String code = "";
	private String type = "";
	private String description = "";
	private Date firstDetectionDate = new Date();
	private int maxClaimPeriodinDays = 0;
	private String diagnosisJETEligibilityStatus = "";

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getFirstDetectionDate() {
		return firstDetectionDate;
	}

	public void setFirstDetectionDate(Date firstDetectionDate) {
		this.firstDetectionDate = firstDetectionDate;
	}

	public int getMaxClaimPeriodinDays() {
		return maxClaimPeriodinDays;
	}

	public void setMaxClaimPeriodinDays(int maxClaimPeriodinDays) {
		this.maxClaimPeriodinDays = maxClaimPeriodinDays;
	}

	/**
	 * @return the diagnosisJETEligibilityStatus
	 */
	public 	@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus")String getDiagnosisJETEligibilityStatus() {
		return diagnosisJETEligibilityStatus;
	}

	/**
	 * @param diagnosisJETEligibilityStatus
	 *            the diagnosisJETEligibilityStatus to set
	 */
	public void setDiagnosisJETEligibilityStatus(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus")String diagnosisJETEligibilityStatus) {
		this.diagnosisJETEligibilityStatus = diagnosisJETEligibilityStatus;
	}
}
