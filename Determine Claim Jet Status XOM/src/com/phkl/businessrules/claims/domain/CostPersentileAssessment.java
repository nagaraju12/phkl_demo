package com.phkl.businessrules.claims.domain;

import java.util.ArrayList;
import java.util.List;

public class CostPersentileAssessment {

	private List<String> treatmentCodes = new ArrayList<String>();
	private String diagnosisCode = "";
	private String medicalCenterGeoArea ="";
	private double treatmentCost50thPercentile=0.0;
	private double treatmentCost75thPercentile=0.0;
	private double treatmentCost90thPercentile=0.0;
	private String medicalCenterCode="";
	private String currency = "";
	private String medicalCenterType = "";
	public String getMedicalCenterType() {
		return medicalCenterType;
	}
	public void setMedicalCenterType(String medicalCenterType) {
		this.medicalCenterType = medicalCenterType;
	}
	public String getDiagnosisCode() {
		return diagnosisCode;
	}
	public void setDiagnosisCode(String diagnosisCode) {
		this.diagnosisCode = diagnosisCode;
	}
	public String getMedicalCenterGeoArea() {
		return medicalCenterGeoArea;
	}
	public void setMedicalCenterGeoArea(String medicalCenterGeoArea) {
		this.medicalCenterGeoArea = medicalCenterGeoArea;
	}
	public String getMedicalCenterCode() {
		return medicalCenterCode;
	}
	public void setMedicalCenterCode(String medicalCenterCode) {
		this.medicalCenterCode = medicalCenterCode;
	}
	public double getTreatmentCost50thPercentile() {
		return treatmentCost50thPercentile;
	}
	public void setTreatmentCost50thPercentile(double treatmentCost50thPercentile) {
		this.treatmentCost50thPercentile = treatmentCost50thPercentile;
	}
	public double getTreatmentCost75thPercentile() {
		return treatmentCost75thPercentile;
	}
	public void setTreatmentCost75thPercentile(double treatmentCost75thPercentile) {
		this.treatmentCost75thPercentile = treatmentCost75thPercentile;
	}
	public double getTreatmentCost90thPercentile() {
		return treatmentCost90thPercentile;
	}
	public void setTreatmentCost90thPercentile(double treatmentCost90thPercentile) {
		this.treatmentCost90thPercentile = treatmentCost90thPercentile;
	}
	public List<String> getTreatmentCodes() {
		if(treatmentCodes == null)
			treatmentCodes = new ArrayList<String>();
		return treatmentCodes;
	}
	public void setTreatmentCodes(List<String> treatmentCodes) {
		this.treatmentCodes = treatmentCodes;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
}
