package com.phkl.businessrules.claims.domain;

public class LOSPersentile {

	private String treatmentCode;
	private String diagnosisCode;
	private String medicalCenterGeoArea;
	private int treatmentLOS50thPercentile;
	private int treatmentLOS75thPercentile;
	private int treatmentLOS90thPercentile;
	public String getTreatmentCode() {
		return treatmentCode;
	}
	public void setTreatmentCode(String treatmentCode) {
		this.treatmentCode = treatmentCode;
	}
	public String getDiagnosisCode() {
		return diagnosisCode;
	}
	public void setDiagnosisCode(String diagnosisCode) {
		this.diagnosisCode = diagnosisCode;
	}
	public String getMedicalCenterGeoArea() {
		return medicalCenterGeoArea;
	}
	public void setMedicalCenterGeoArea(String medicalCenterGeoArea) {
		this.medicalCenterGeoArea = medicalCenterGeoArea;
	}
	public int getTreatmentLOS50thPercentile() {
		return treatmentLOS50thPercentile;
	}
	public void setTreatmentLOS50thPercentile(int treatmentLOS50thPercentile) {
		this.treatmentLOS50thPercentile = treatmentLOS50thPercentile;
	}
	public int getTreatmentLOS75thPercentile() {
		return treatmentLOS75thPercentile;
	}
	public void setTreatmentLOS75thPercentile(int treatmentLOS75thPercentile) {
		this.treatmentLOS75thPercentile = treatmentLOS75thPercentile;
	}
	public int getTreatmentLOS90thPercentile() {
		return treatmentLOS90thPercentile;
	}
	public void setTreatmentLOS90thPercentile(int treatmentLOS90thPercentile) {
		this.treatmentLOS90thPercentile = treatmentLOS90thPercentile;
	}
	
}
