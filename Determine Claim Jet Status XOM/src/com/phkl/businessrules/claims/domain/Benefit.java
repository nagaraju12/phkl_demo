package com.phkl.businessrules.claims.domain;

import ilog.rules.bom.annotations.BusinessType;

import java.util.Date;

public class Benefit {
	private int benefitCode;
	@BusinessType("com.phkl.businessrules.claims.virtualdomain.BenefitType")
	private String benefitType;
	private Date benefitDate;
	public int getBenefitCode() {
		return benefitCode;
	}
	public void setBenefitCode(int benefitCode) {
		this.benefitCode = benefitCode;
	}
	public Date getBenefitDate() {
		return benefitDate;
	}
	public void setBenefitDate(Date benefitDate) {
		this.benefitDate = benefitDate;
	}
	public @BusinessType("com.phkl.businessrules.claims.virtualdomain.BenefitType") String getBenefitType() {
		return benefitType;
	}
	public void setBenefitType(@BusinessType("com.phkl.businessrules.claims.virtualdomain.BenefitType") String benefitType) {
		this.benefitType = benefitType;
	}
	
	

}
