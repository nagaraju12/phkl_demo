package com.phkl.businessrules.claims.domain;

public class ClaimRejectionDetail {
	private String claimID = "";
	private String policyID = "";
	private String diagnosisCode = "";
	private String code="";
	private String description="";
	public String getDiagnosisCode() {
		return diagnosisCode;
	}

	public void setDiagnosisCode(String diagnosisCode) {
		this.diagnosisCode = diagnosisCode;
	}


	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the claimID
	 */
	public String getClaimID() {
		return claimID;
	}

	/**
	 * @param claimID
	 *            the claimID to set
	 */
	public void setClaimID(String claimID) {
		this.claimID = claimID;
	}

	/**
	 * @return the policyID
	 */
	public String getPolicyID() {
		return policyID;
	}

	/**
	 * @param policyID
	 *            the policyID to set
	 */
	public void setPolicyID(String policyID) {
		this.policyID = policyID;
	}

}
