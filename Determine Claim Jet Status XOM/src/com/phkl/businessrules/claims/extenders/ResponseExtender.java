package com.phkl.businessrules.claims.extenders;

import ilog.rules.bom.annotations.BusinessType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.phkl.businessrules.claims.domain.ClaimRejectionDetail;
import com.phkl.businessrules.claims.domain.CostPersentileAssessment;
import com.phkl.businessrules.claims.domain.LOSPersentile;
import com.phkl.businessrules.claims.domain.MedicalCenter;
import com.phkl.businessrules.claims.domain.Treatment;
import com.phkl.businessrules.claims.jetStatus.DetermineClaimJetStatusResponse;

public class ResponseExtender {

	public static List<String> getUniqueMedicalCenterGeoraphicAreas(
			List medicalCenterGeographicAreas) {
		if (null == medicalCenterGeographicAreas)
			return new ArrayList<String>();

		return new ArrayList<String>(new HashSet<String>(
				medicalCenterGeographicAreas));
	}

	// add the geographic area of {0} to response{1}
	public static void addMedicalCenterGeoArea(MedicalCenter medCenter,
			DetermineClaimJetStatusResponse response) {
		if (medCenter != null
				&& (medCenter.getGeographicArea() != null && !medCenter
						.getGeographicArea().isEmpty()))

			response.getMedicalCenterGeographicAreas().add(
					medCenter.getGeographicArea());
	}

	// bList contains aList
	public static boolean contains(List<String> aList, List<String> bList) {
		if (bList != null && aList != null)
			return bList.containsAll(aList);

		return false;

	}

	public static void addPolicyExclusionWithRejectionCode(
			String rejectionCode, String diagnosisCode, String ExclusionStatus,
			DetermineClaimJetStatusResponse response) {

		ClaimRejectionDetail rejectDetail = new ClaimRejectionDetail();
		rejectDetail.setCode(rejectionCode);
		rejectDetail.setDiagnosisCode(diagnosisCode);
		response.getClaimRejectionDetails().add(rejectDetail);
	}

	public static void addSurgicalCodes(
			Treatment treatment,
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.SurgicalCodeSeverity") String severity,
			String description, DetermineClaimJetStatusResponse response) {
		if (severity != null && treatment != null) {
			treatment.setDescription(description);
			if (severity.equals("Severity 1"))
				response.getSeverity1SurgicalCodes().add(treatment);
			else if (severity.equals("Severity 2"))
				response.getSeverity2SurgicalCodes().add(treatment);
			else if (severity.equals("Severity 3"))
				response.getSeverity3SurgicalCodes().add(treatment);
			else if (severity.equals("Severity 4"))
				response.getSeverity4SurgicalCodes().add(treatment);
			else if (severity.equals("Severity 5"))
				response.getSeverity5SurgicalCodes().add(treatment);
			else if (severity.equals("Severity 6"))
				response.getSeverity6SurgicalCodes().add(treatment);
			else if (severity.equals("Severity 7"))
				response.getSeverity7SurgicalCodes().add(treatment);

		}

	}

	public static void addSurgeryTreatmentAssessment(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.SurgeryAppropriateness") String treatmentAppropriateness,
			DetermineClaimJetStatusResponse response) {

		if (treatmentAppropriateness != null
				&& !treatmentAppropriateness.isEmpty())
			response.getSurgeryAssessmentList().add(treatmentAppropriateness);
	}

	public static void addLOSPercentileToResponse(String treatmentCode,
			String diagnosisCode, String medicalCenterGeoArea, int per50,
			int per75, int per90, DetermineClaimJetStatusResponse res) {

		LOSPersentile losPercentile = new LOSPersentile();
		losPercentile.setDiagnosisCode(diagnosisCode);
		losPercentile.setTreatmentCode(treatmentCode);
		losPercentile.setMedicalCenterGeoArea(medicalCenterGeoArea);
		losPercentile.setTreatmentLOS50thPercentile(per50);
		losPercentile.setTreatmentLOS75thPercentile(per75);
		losPercentile.setTreatmentLOS90thPercentile(per90);
		res.getTreatmentLOSAssessmentList().add(losPercentile);

	}

	public static void addMedicalWastageAssessList(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.WastageAssessment") String wastageAssessment,
			DetermineClaimJetStatusResponse res) {

		if (res.getClaimMedicalWastageAssessmentList() != null)
			res.getClaimMedicalWastageAssessmentList().add(wastageAssessment);

	}

	public static void addCostAssessmenttoResponse(List<String> treatmentCodes,
			String medicalCenterGeoArea, String medicalCenterCode,String medicalCenterType,
			double per50, double per75, double per90, String currency,
			DetermineClaimJetStatusResponse res) {
		CostPersentileAssessment cps = null;
		boolean added = false;
		for (CostPersentileAssessment cp : res.getClaimCostPercentileList()) {
			if (cp.getTreatmentCodes().containsAll(treatmentCodes))
				added = true;
			break;
		}
		if (added == false) {
			cps = new CostPersentileAssessment();
			cps.getTreatmentCodes().addAll(treatmentCodes);
			cps.setMedicalCenterGeoArea(medicalCenterGeoArea);
			cps.setMedicalCenterCode(medicalCenterCode);
			cps.setMedicalCenterType(medicalCenterType);
			cps.setTreatmentCost50thPercentile(per50);
			cps.setTreatmentCost75thPercentile(per75);
			cps.setTreatmentCost90thPercentile(per90);
			cps.setCurrency(currency);
			res.getClaimCostPercentileList().add(cps);
		}
	}

	public static void addSurgerytoSurgeryList(String treatmentCode,
			DetermineClaimJetStatusResponse res) {
		if (treatmentCode != null && !treatmentCode.isEmpty())
			res.getSurgeryList().add(treatmentCode);
	}

	public static String getAllSurgicalCodes(DetermineClaimJetStatusResponse res) {

		StringBuffer sb = null;
		if (res.getSurgeryList() != null && res.getSurgeryList().size() > 0) {
			sb = new StringBuffer();
			for (int i = 0; i < res.getSurgeryList().size(); i++) {
				sb.append(res.getSurgeryList().get(i));
				sb.append(" ");

			}
		}
		return sb.toString();
	}

	// public static void main(String[] args) {
	//
	// ArrayList<String> abc = new ArrayList<>();
	// abc.add("A");
	// abc.add("A");
	// abc.add("A");
	// System.out.println(abc.toArray().toString());
	//
	//
	//
	//
	// }

	// public static int getMaxDuration(String treatmentCode, String
	// diagnosisCode, String medicalCenterGeoArea, String paramToCheck,
	// DetermineClaimJetStatusResponse res){
	//
	// int maxDuration = 0 ;
	// List<Integer> percentileList = null;
	// if(res.getTreatmentLOSAssessmentList() != null &&
	// res.getTreatmentLOSAssessmentList().size() > 0){
	// percentileList = new ArrayList<Integer>();
	// for(int i=0; i< res.getTreatmentLOSAssessmentList().size() ;i++ ){
	// LOSPersentile item = res.getTreatmentLOSAssessmentList().get(i);
	// if((item.getDiagnosisCode()!= null &&
	// item.getDiagnosisCode().equals(diagnosisCode)) &&
	// (item.getTreatmentCode()diagnosisCode != null &&
	// item.getTreatmentCode().equals(treatmentCode)))
	//
	//
	// }
	//
	//
	//
	// }
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	// return maxDuration;
	// }
	//

}
