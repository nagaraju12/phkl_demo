package com.phkl.businessrules.claims.extenders;

import ilog.rules.bom.annotations.BusinessType;

import com.phkl.businessrules.claims.domain.Policy;
import com.phkl.businessrules.claims.jetStatus.DetermineClaimJetStatusResponse;

public class ProductExtender {

	public static boolean productEligibilityTest(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String prodStatus,
			Policy policy) {

		if (prodStatus == null
				|| policy == null
				|| (policy != null && (policy.getProducts() == null || (policy
						.getProducts() != null && policy.getProducts().size() == 0))))
			return false;

		for (int i = 0; i < policy.getProducts().size(); i++) {
			if (policy.getProducts().get(i).getJetEligibility() != null
					&& policy.getProducts().get(i).getJetEligibility()
							.equals(prodStatus))
				return true;
		}

		return false;
	}

	public static boolean diagnosisEligibilityTest(
			@BusinessType("com.phkl.businessrules.claims.virtualdomain.EligibilityStatus") String diagnosisStatus,
			DetermineClaimJetStatusResponse response) {

		if (diagnosisStatus == null || response == null
				|| (null != response && response.getAllDiagnosis() == null))
			return false;
		if (response.getAllDiagnosis().size() >= 1) {
			for (int i = 0; i < response.getAllDiagnosis().size(); i++) {
				if (response.getAllDiagnosis().get(i) == null
						|| (response.getAllDiagnosis().get(i) != null && response
								.getAllDiagnosis().get(i)
								.getDiagnosisJETEligibilityStatus() == null))
					return false;
				if (response.getAllDiagnosis().get(i)
						.getDiagnosisJETEligibilityStatus()
						.equals("JET Ineligible"))
					return true;
			}
		}
		return false;
	}
}
