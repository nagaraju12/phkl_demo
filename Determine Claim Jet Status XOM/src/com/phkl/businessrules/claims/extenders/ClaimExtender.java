package com.phkl.businessrules.claims.extenders;

import ilog.rules.bom.annotations.BusinessType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.phkl.businessrules.claims.common.ExchangeRate;
import com.phkl.businessrules.claims.domain.Claim;
import com.phkl.businessrules.claims.domain.Diagnosis;
import com.phkl.businessrules.claims.domain.Product;
//import com.phkl.businessrules.claims.domain.TreatmentCode;
import com.phkl.businessrules.claims.jetStatus.DetermineClaimJetStatusRequest;
import com.phkl.businessrules.claims.jetStatus.DetermineClaimJetStatusResponse;
import com.phkl.businessrules.common.utility.DateUtility;

//import com.phkl.businessrules.common.utility.DateUtility;

public class ClaimExtender {

	public static void addDiagnosisCode(Diagnosis diagnosis, String indicator, @BusinessType("com.phkl.businessrules.claims.virtualdomain.DiagnosisType")String diagnosisType,
			DetermineClaimJetStatusResponse response) {

		if (null != diagnosisType)
			diagnosis.setType(diagnosisType);
		if ((!(indicator == null) && (indicator != null || !indicator.isEmpty())) && indicator.equals("Y") && diagnosisType.equals("Acute"))
			response.getAcuteDiagnosisList().add(diagnosis);
		else if ((!(indicator == null) && (indicator != null || !indicator.isEmpty())) && indicator.equals("Y")
				&& diagnosisType.equals("Acute Only"))
			response.getAcuteOnlyDiagnosisList().add(diagnosis);
		else if ((!(indicator == null) && (indicator != null || !indicator.isEmpty())) && indicator.equals("Y")
				&& diagnosisType.equals("Chronic"))
			response.getChronicDiagnosisList().add(diagnosis);
		else if ((!(indicator == null) && (indicator != null || !indicator.isEmpty())) && indicator.equals("Y")
				&& diagnosisType.equals("Catastrophic"))
			response.getCatastrophicDiagnosisList().add(diagnosis);
	}
	
	public static void classifyProducts(Product product, DetermineClaimJetStatusResponse response) {
		
		if(product!= null && product.getCode()!= null && product.getJetEligibility() != null ){
			if(product.getJetEligibility().equals("JET Eligible"))
				response.getEligibleProductList().add(product);
			
			if(product.getJetEligibility().equals("JET Ineligible"))
				response.getIneligibleProductList().add(product);
		}
		
	}	

	public static void prepareAllDiagnosisList(Diagnosis diagnosis, DetermineClaimJetStatusResponse response) {

		if (diagnosis != null && diagnosis.getCode() != null)
			response.getAllDiagnosis().add(diagnosis);
	}

	public static boolean checkComboExistance(String aCode, String bCode, List<Diagnosis> diagnosisList) {
		boolean aFound = false;
		boolean bFound = false;
		String item = "";
		if (null == diagnosisList || (null != diagnosisList && diagnosisList.size() < 2)
				|| (null != diagnosisList && diagnosisList.size() > 2))
			return false;
		for (int i = 0; i < diagnosisList.size(); i++) {
			if (null != diagnosisList.get(i).getCode())
				item = diagnosisList.get(i).getCode();
			if (item.equals(aCode))
				aFound = true;
			if (aFound)
				break;
		}
		for (int i = 0; i < diagnosisList.size(); i++) {
			if (null != diagnosisList.get(i).getCode())
				item = diagnosisList.get(i).getCode();
			if (item.equals(bCode))
				bFound = true;
			if (bFound)
				break;
		}
		return (aFound && bFound);
	}

	public static int getClaimEventCountbyType(@BusinessType("com.phkl.businessrules.claims.virtualdomain.EventType")String eventType, Claim claim) {
		int count = 0;
		if (null == claim.getClaimEvents() || (null != claim.getClaimEvents() && claim.getClaimEvents().size() == 0))
			return 0;
		if (null != claim.getClaimEvents() && claim.getClaimEvents().size() > 0) {
			for (int i = 0; i < claim.getClaimEvents().size(); i++) {
				if (null != claim.getClaimEvents().get(i)) {
					if (null != claim.getClaimEvents().get(i).getEventType()) {
						if (eventType.equals(claim.getClaimEvents().get(i).getEventType()))
							count++;
					}
				}

			}

		}
		return count;
	}

	public static int getSeverityCodeCountByLevel(String severity, DetermineClaimJetStatusResponse response) {
		if (severity.equals("Severity 1")) {
			if (response.getSeverity1SurgicalCodes() == null)
				return 0;
			response.getSeverity1SurgicalCodes().size();
		} else if (severity.equals("Severity 2")) {
			if (response.getSeverity2SurgicalCodes() == null)
				return 0;
			response.getSeverity2SurgicalCodes().size();
		} else if (severity.equals("Severity 3")) {
			if (response.getSeverity3SurgicalCodes() == null)
				return 0;
			response.getSeverity3SurgicalCodes().size();
		} else if (severity.equals("Severity 4")) {
			if (response.getSeverity4SurgicalCodes() == null)
				return 0;
			response.getSeverity4SurgicalCodes().size();
		} else if (severity.equals("Severity 5")) {
			if (response.getSeverity5SurgicalCodes() == null)
				return 0;
			response.getSeverity5SurgicalCodes().size();
		} else if (severity.equals("Severity 6")) {
			if (response.getSeverity6SurgicalCodes() == null)
				return 0;
			response.getSeverity6SurgicalCodes().size();
		} else if (severity.equals("Severity 7")) {
			if (response.getSeverity7SurgicalCodes() == null)
				return 0;
			response.getSeverity7SurgicalCodes().size();
		}

		return 0;

	}

	public static int getClaimCountWithinGivenYearsFromADate(int year, Date aDate, DetermineClaimJetStatusRequest request) {
		Date earliestClaimEventDate = new Date();
		Date nYearBackFromEarliesEventDate = new Date();
		Date previousClaimLastEventDate = new Date();
		Claim previousClaim = null;
		int claimsCount = 0;
		if (aDate == null || request.getCurrentClaim() == null)
			return 0;
		if (null == request.getPreviousClaims() || (null != request.getPreviousClaims() && request.getPreviousClaims().size() <= 0))
			return 0;
		if (null == request.getCurrentClaim().getClaimEvents()
				|| (null != request.getCurrentClaim().getClaimEvents() && request.getCurrentClaim().getClaimEvents().size() == 0))
			return 0;

		earliestClaimEventDate = aDate;
		nYearBackFromEarliesEventDate = DateUtility.getDateBeforeYears(year, earliestClaimEventDate);

		for (int i = 0; i < request.getPreviousClaims().size(); i++) {
			// if Previous Claims Exist
			if (null != request.getPreviousClaims().get(i))
				previousClaim = request.getPreviousClaims().get(i);
			// Previous Claim has Claim Events
			if (null != previousClaim.getClaimEvents() && previousClaim.getClaimEvents().size() > 0) {
				// Get latest claim event from ith Previous claim
				previousClaimLastEventDate = getClaimLastEventDate(previousClaim);
				// Previous Claim Last Event Date is Between 2 years back date
				// and current claim earliest event date
				if (previousClaimLastEventDate != null
						&& (previousClaimLastEventDate.compareTo(nYearBackFromEarliesEventDate) > 0 && previousClaimLastEventDate
								.compareTo(earliestClaimEventDate) < 0))
					claimsCount++;
			}
		}
		return claimsCount;
	}

	public static Date getClaimEarliestEventDate(Claim claim) {

		List<Date> currentClaimEventsDateList = null;
		if (claim == null)
			return new Date();
		if (claim != null && (null == claim.getClaimEvents() || (null != claim.getClaimEvents() && claim.getClaimEvents().size() == 0)))
			return claim.getSubmissionDate();

		if (claim.getClaimEvents() != null && claim.getClaimEvents().size() > 0) {
			currentClaimEventsDateList = new ArrayList<Date>();
			for (int i = 0; i < claim.getClaimEvents().size(); i++) {
				if (null != claim.getClaimEvents().get(i) && null != claim.getClaimEvents().get(i).getEventStartDate())
					currentClaimEventsDateList.add(claim.getClaimEvents().get(i).getEventStartDate());
			}
		}

		if (currentClaimEventsDateList != null && currentClaimEventsDateList.size() > 0)
			return Collections.min(currentClaimEventsDateList);

		return claim.getSubmissionDate();

	}

	public static Date getClaimLastEventDate(Claim claim) {

		List<Date> currentClaimEventsDateList = null;
		if (claim == null)
			return new Date();
		if (claim != null && (null == claim.getClaimEvents() || (null != claim.getClaimEvents() && claim.getClaimEvents().size() == 0)))
			return claim.getSubmissionDate();

		if (claim.getClaimEvents() != null && claim.getClaimEvents().size() > 0) {
			currentClaimEventsDateList = new ArrayList<Date>();
			for (int i = 0; i < claim.getClaimEvents().size(); i++) {
				if (null != claim.getClaimEvents().get(i) && null != claim.getClaimEvents().get(i).getEventStartDate())
					currentClaimEventsDateList.add(claim.getClaimEvents().get(i).getEventStartDate());
				
			}
		}

		if (currentClaimEventsDateList != null && currentClaimEventsDateList.size() > 0)
			return Collections.max(currentClaimEventsDateList);

		return claim.getSubmissionDate();

	}

	public static double getConvertedAmountinHKD(ExchangeRate exRate, double claimBilledAmount, String claimBilledAmountCurrency) {
		if (claimBilledAmountCurrency == null || (claimBilledAmountCurrency != null && claimBilledAmountCurrency.isEmpty())
				|| claimBilledAmount == 0)
			return 0.0;
		if ((exRate != null && exRate.getFromCurrency() != null) && (claimBilledAmountCurrency.equals(exRate.getFromCurrency()))) {
			return claimBilledAmount * exRate.getRate();

		}
		return claimBilledAmount;
	}
//	public static void main(String[] args) {
//		
//		String curency = null;
//		double value = 10000;
//		ExchangeRate exRate = new ExchangeRate();
//		
//		exRate.setFromCurrency("USD");
//		exRate.setRate(0.33);
//		
//		System.out.println(getConvertedAmountinHKD(exRate, value, curency));
//		
//		
//		
//		
//	}
//	
	
	
	
	//
	// public static void main(String[] args) {
	// // ClaimJetEligibilityRequest req = new ClaimJetEligibilityRequest();
	// DetermineClaimJetStatusRequest req = new
	// DetermineClaimJetStatusRequest();
	// Claim currentClaim = new Claim();
	// Claim pClaim1 = new Claim();
	// Claim pClaim2 = new Claim();
	// Claim pClaim3 = new Claim();
	// Claim pClaim4 = new Claim();
	//
	// ClaimEvent event1 = new ClaimEvent();
	// ClaimEvent event2 = new ClaimEvent();
	// ClaimEvent event3 = new ClaimEvent();
	// ClaimEvent event4 = new ClaimEvent();
	//
	// Calendar cal = Calendar.getInstance();
	// cal.set(1991, 0, 13);
	// event1.setEventStartDate(cal.getTime());
	// cal.set(1990, 8, 8);
	// event2.setEventStartDate(cal.getTime());
	// cal.set(1992, 2, 21);
	// event3.setEventStartDate(cal.getTime());
	// cal.set(1989, 8, 4);
	// //event4.setEventStartDate(cal.getTime());
	// event4.setEventStartDate(null);
	//
	// List<ClaimEvent> curClaimEvents = new ArrayList<ClaimEvent>();
	// curClaimEvents.add(event1);
	// curClaimEvents.add(event2);
	// curClaimEvents.add(event3);
	// curClaimEvents.add(event4);
	// currentClaim.getClaimEvents().addAll(curClaimEvents);
	//
	// System.out.println(getClaimEarliestEventDate(currentClaim));
	//
	//
	// List<Claim> previousClaims = new ArrayList<Claim>();
	//
	// ClaimEvent pcEvent1 = new ClaimEvent();
	// ClaimEvent pcEvent2 = new ClaimEvent();
	// ClaimEvent pcEvent3 = new ClaimEvent();
	// ClaimEvent pcEvent4 = new ClaimEvent();
	//
	// cal.set(1989, 1, 1);
	// pcEvent1.setEventStartDate(cal.getTime());
	// cal.set(1986, 2, 2);
	// //pcEvent2.setEventStartDate(cal.getTime());
	// cal.set(1989, 3, 3);
	// pcEvent3.setEventStartDate(cal.getTime());
	// cal.set(1989, 11, 4);
	// pcEvent4.setEventStartDate(cal.getTime());
	//
	//
	//
	// pClaim1.getClaimEvents().add(pcEvent1);
	// pClaim2.getClaimEvents().add(pcEvent2);
	// //pClaim3.getClaimEvents().add(pcEvent3);
	// pClaim4.getClaimEvents().add(pcEvent4);
	//
	//
	// //
	// // previousClaims.add(pClaim1);
	// // previousClaims.add(pClaim2);
	// // previousClaims.add(pClaim3);
	// // //previousClaims.add(pClaim4);
	//
	//
	// req.getPreviousClaims().add(pClaim1);
	// req.getPreviousClaims().add(pClaim2);
	// req.getPreviousClaims().add(pClaim3);
	// req.getPreviousClaims().add(pClaim4);
	//
	// // req.setCurrentClaim(currentClaim);
	//
	// System.out.println(ClaimExtender.getClaimCountWithinGivenYearsFromADate(2,
	// ClaimExtender.getClaimEarliestEventDate(currentClaim),
	// req));
	//
	// }

	// public static void main(String[] args) {
	//
	// Claim claim = new Claim();
	// Treatment t1 = new Treatment();
	// Treatment t2 = new Treatment();
	//
	// t1.setType("Inpatient");
	// t2.setType("Inpatient");
	//
	// claim.getTreatments().add(t1);
	// claim.getTreatments().add(t2);
	//
	// System.out.println(ClaimExtender.getClaimEventCountbyType("Inpatient",
	// claim));
	//
	// }

}
