package com.phkl.businessrules.common.utility;

import java.util.List;

public class CommonUtility {

	public static int getSizeOf(List aList) {
		if (aList != null)
			return aList.size();
		return 0;
	}

	public static String asString(Object object) {
		String value = "";
		if (object == null)
			return value;
		value = (String) object.toString();
		return value;
	}

}
