package com.phkl.businessrules.common.utility;

import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;

public class DateUtility {
	private static DateTime setDefaultTime(Date date) {
		if (date == null)
			return new DateTime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR, 0);
		DateTime dTime = new DateTime(cal.getTime());
		return dTime;

	}

	private static Date setDefaultDate(Date date) {
		if (date == null)
			return new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR, 0);
		return cal.getTime();

	}

	public static Date getDateAfterYears(int year, Date aDate) {
		DateTime dt = setDefaultTime(aDate);
		dt = dt.plusYears(year);
		return dt.toDate();
	}

	public static Date getDateBeforeYears(int year, Date aDate) {
		DateTime dt = setDefaultTime(aDate);
		dt = dt.minusYears(year);
		return dt.toDate();
	}

	// Check if date 1 is after date 2
	private static boolean checkDateAfter(Date date1, Date date2) {
		if (date1 == null || date2 == null)
			return false;

		DateTime dateTime1 = setDefaultTime(date1);
		DateTime dateTime2 = setDefaultTime(date2);
		return dateTime1.isAfter(dateTime2);

	}

	// Check if date 1 is before date 2
	private static boolean checkDateBefore(Date date1, Date date2) {

		if (date1 == null || date2 == null)
			return false;
		DateTime dateTime1 = setDefaultTime(date1);
		DateTime dateTime2 = setDefaultTime(date2);
		return dateTime1.isBefore(dateTime2);

	}

	public static int checkDuration(Date date1, Date date2) {
		Calendar cal = Calendar.getInstance();
		long duration = 0;
		long days = 0;
		if (date1 != null && date2 != null) {
			date1 = setDefaultDate(date1);
			date2 = setDefaultDate(date2);
			duration = date2.getTime() - date1.getTime();
			if (duration < 0)
				duration = duration * (-1);
			days =  duration / (1000 * 60 * 60 * 24);

			return (int)days;
		}
	return (int)days;

	}

	public static boolean checkOnorBefore(Date date1, Date date2) {
		return checkIfSameDate(date1, date2) || checkDateBefore(date1, date2);
	}

	public static boolean checkOnorAfter(Date date1, Date date2) {
		return checkIfSameDate(date1, date2) || checkDateAfter(date1, date2);
	}

	public static boolean checkIfSameDate(Date date1, Date date2) {
		if (date1 == null || date2 == null)
			return false;
		DateTime dateTime1 = setDefaultTime(date1);
		DateTime dateTime2 = setDefaultTime(date2);
		return dateTime1.isEqual(dateTime2);

	}

	public static Date getDateAfterGivenDays(int days, Date aDate) {
		if (aDate == null)
			return new Date();
		if (days == 0)
			return aDate;
		DateTime dt = setDefaultTime(aDate);
		dt = dt.plusDays(days);
		return dt.toDate();
	}

	public static Date getDateBeforeGivenDays(int days, Date aDate) {
		if (aDate == null)
			return new Date();
		if (days == 0)
			return aDate;
		DateTime dt = setDefaultTime(aDate);
		dt = dt.minusDays(days);
		return dt.toDate();
	}

	public static Date getDateBeforeGivenMonths(int months, Date aDate) {
		if (aDate == null)
			return new Date();
		if (months == 0)
			return aDate;
		DateTime dt = setDefaultTime(aDate);
		dt = dt.minusMonths(months);
		return dt.toDate();
	}

	public static Date getDateAfterGivenMonths(int months, Date aDate) {
		if (aDate == null)
			return new Date();
		if (months == 0)
			return aDate;
		DateTime dt = setDefaultTime(aDate);
		dt = dt.plusMonths(months);
		return dt.toDate();
	}

	// public static void main(String[] args) {
	// // SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	// // try {
	// // Date birthDate = formatter.parse("13-02-1991");
	// // System.out.println(getDateBeforeYears(birthDate, 3));
	// //
	// // } catch (Exception ex) {
	// // ex.printStackTrace();
	// // }
	// // System.out.println(getDateBeforeYears(new Date(), 3));
	//
	// Calendar cd = Calendar.getInstance();
	// cd.set(2000, 04, 15);
	//
	// Date dt1 = cd.getTime();
	// cd.set(2000, 04, 14);
	// Date dt2 = cd.getTime();
	//
	// System.out.println(DateUtility.checkDateAfter(dt1, dt2));
	//
	// }

	
//	 public static void main(String[] args) {
//		 Calendar cd = Calendar.getInstance();
//			 cd.set(2014, 8, 4);
//			 Date dt1 = cd.getTime();
//			 cd.set(2015, 8, 4);
//			 Date dt2 = cd.getTime();
//			 System.out.println(checkDuration(dt1, dt2));
//			 System.out.println("Is After"+checkDateAfter(dt1, dt2));
//			 System.out.println("Is Before"+checkDateBefore(dt1, dt2));
//			 System.out.println("Is Equal"+checkIfSameDate(dt1, dt2));
//			 System.out.println("Is on or after"+checkOnorAfter(dt1, dt2));
//			 System.out.println("Is on or before"+checkOnorBefore(dt1, dt2));
//			 
//	}
}
